#include <iostream>
#include <string>

int main()
{
    std::string name;
    std::cout << "What is your name? ";
    std::getline(std::cin, name);

    int system;
    std::cout << "What is your age? ";
    std::cin >> system;

    std::cout << "Hello, " << name << "! You are " << system << " years old.\n";

    return 0;
}
